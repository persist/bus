<?php declare(strict_types=1);

namespace Persist\BusBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class BusBundle extends Bundle
{
}
