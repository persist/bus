<?php declare(strict_types=1);

namespace Persist\BusBundle\QueryBus;

use League\Tactician\CommandBus;

class QueryBus implements QueryBusInterface
{
    /** @var CommandBus */
    private $commandBus;

    public function __construct(CommandBus $commandBus)
    {
        $this->commandBus = $commandBus;
    }

    public function handle($query)
    {
        return $this->commandBus->handle($query);
    }
}
