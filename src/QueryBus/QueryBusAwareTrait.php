<?php declare(strict_types=1);

namespace Persist\BusBundle\QueryBus;

trait QueryBusAwareTrait
{
    /** @var QueryBusInterface */
    protected $queryBus;

    protected function getQueryBus(): QueryBusInterface
    {
        return $this->queryBus;
    }

    public function setQueryBus(QueryBusInterface $queryBus): void
    {
        $this->queryBus = $queryBus;
    }

    protected function handleQuery($query)
    {
        return $this->getQueryBus()->handle($query);
    }
}
