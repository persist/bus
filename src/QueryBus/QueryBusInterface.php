<?php declare(strict_types=1);

namespace Persist\BusBundle\QueryBus;

interface QueryBusInterface
{
    public function handle($query);
}
