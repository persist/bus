<?php declare(strict_types=1);

namespace Persist\BusBundle\CommandBus;

trait CommandBusAwareTrait
{
    /** @var CommandBusInterface */
    protected $commandBus;

    protected function getCommandBus(): CommandBusInterface
    {
        return $this->commandBus;
    }

    public function setCommandBus(CommandBusInterface $commandBus): void
    {
        $this->commandBus = $commandBus;
    }

    protected function handleCommand($command): void
    {
        $this->getCommandBus()->handle($command);
    }
}
