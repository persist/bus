<?php declare(strict_types=1);

namespace Persist\BusBundle\CommandBus;

interface CommandBusInterface
{
    public function handle($command): void;
}
