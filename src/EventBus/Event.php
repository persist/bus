<?php declare(strict_types=1);

namespace Persist\BusBundle\EventBus;

abstract class Event extends \Symfony\Component\EventDispatcher\Event
{
    public function getEventName(): string
    {
        return static::class;
    }
}
