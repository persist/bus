<?php declare(strict_types=1);

namespace Persist\BusBundle\EventBus;

use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class EventBus implements EventBusInterface
{
    /** @var EventDispatcherInterface */
    private $dispatcher;

    public function __construct(EventDispatcherInterface $dispatcher)
    {
        $this->dispatcher = $dispatcher;
    }

    public function dispatch(Event $event): void
    {
        $this->dispatcher->dispatch($event->getEventName(), $event);
    }
}
