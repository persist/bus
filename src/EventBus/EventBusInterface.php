<?php declare(strict_types=1);

namespace Persist\BusBundle\EventBus;

interface EventBusInterface
{
    public function dispatch(Event $event): void;
}
