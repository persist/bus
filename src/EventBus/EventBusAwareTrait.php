<?php declare(strict_types=1);

namespace Persist\BusBundle\EventBus;

trait EventBusAwareTrait
{
    /** @var EventBusInterface */
    protected $eventBus;

    protected function getEventBus(): EventBusInterface
    {
        return $this->eventBus;
    }

    public function setEventBus(EventBusInterface $eventBus)
    {
        $this->eventBus = $eventBus;
    }

    protected function dispatchEvent(Event $event): void
    {
        $this->getEventBus()->dispatch($event);
    }
}
